<?php

namespace Wxl\Captcha\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Wxl\Captcha\Captcha
 */
class Captcha extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'captcha';
    }
}
