<?php

namespace Wxl\Captcha;

use Exception;
use Laravel\Lumen\Routing\Controller;

/**
 * Class CaptchaController
 */
class LumenCaptchaController extends Controller
{
    /**
     * get CAPTCHA
     *
     * @param Captcha $captcha
     * @param string $config
     * @return array
     * @throws Exception
     */
    public function getCaptcha(Captcha $captcha, string $config = 'default'): array
    {
        return $captcha->create($config);
    }
}
