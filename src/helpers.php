<?php

use Intervention\Image\ImageManager;

if (!function_exists('captcha')) {
	/**
	 * @param string $config
	 * @return array|ImageManager|mixed
	 * @throws Exception
	 */
	function captcha(string $config = 'default')
	{
		return app('captcha')->create($config);
	}
}

if (!function_exists('captcha_check')) {
	/**
	 * @param string $value
	 * @param string $key
	 * @param string $config
	 * @return bool
	 */
	function captcha_check(string $value, string $key, string $config = 'default'): bool
	{
		return app('captcha')->check($value, $key, $config);
	}
}

if (! function_exists('config_path')) {
	/**
	 * Get the configuration path.
	 *
	 * @param string $path
	 * @return string
	 */
	function config_path(string $path = ''): string
	{
		return app()->configPath($path);
	}
}